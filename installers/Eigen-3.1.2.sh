#!/bin/bash
mkdir -p ~/opt/src && cd ~/opt/src
git clone https://github.com/eigenteam/eigen-git-mirror eigen
cd eigen 
git checkout 3.1.2
mkdir -p Release && cd Release
cmake .. -DCMAKE_INSTALL_PREFIX=~/opt/eigen-3.1.2 -DCMAKE_BUILD_TYPE=Release
make install
ln -s ~/opt/modules/modules/eigen-3.1.2 ~/opt/modules/installed/eigen-3.1.2
echo "module load eigen-3.1.2" >> $HOME/.bashrc
source $HOME/.bashrc
