#!/bin/bash
mkdir -p $HOME/opt/src && cd $HOME/opt/src
VER=1.73.0
UVER=${VER//./_} #Change "." to "_" 
wget https://boostorg.jfrog.io/artifactory/main/release/$VER/source/boost_$UVER.tar.bz2
tar -xf boost_$UVER.tar.gz && cd boost_$UVER
./bootstrap.sh --prefix=$HOME/opt/boost_$UVER
./b2 install
ln -s ~/opt/modules/modules/boost-$VER ~/opt/modules/installed/boost-$VER

#Clean
cd ..
rm boost_$UVER.tar.gz
rm -r -f boost_$UVER