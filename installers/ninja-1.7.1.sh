#!/bin/bash
# Download, compile and install cmake 3.3.1
NINJA_VERSION=1.7.1
cd $HOME/opt/src
git clone git://github.com/ninja-build/ninja.git && cd ninja
git checkout v${NINJA_VERSION}
./configure.py --bootstrap 
mkdir -p $HOME/opt/ninja-${NINJA_VERSION}
cp ninja $HOME/opt/ninja-${NINJA_VERSION}
echo "module load ninja-${NINJA_VERSION}" >> $HOME/.bashrc
ln -s ~/opt/modules/modules/ninja-${NINJA_VERSION} ~/opt/modules/installed/ninja-${NINJA_VERSION}
source $HOME/.bashrc
