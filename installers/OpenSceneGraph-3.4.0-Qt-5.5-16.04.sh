#!/bin/bash
cd $HOME/opt
wget http://gmrv.es/~ptoharia/OpenSceneGraph-3.4.0-Qt5.5-16.04.tgz
tar zxvf OpenSceneGraph-3.4.0-Qt5.5-16.04.tgz
rm OpenSceneGraph-3.4.0-Qt5.5-16.04.tgz
ln -s ~/opt/modules/modules/OpenSceneGraph-3.4.0-Qt5.5-16.04 ~/opt/modules/installed/OpenSceneGraph-3.4.0-Qt5.5-16.04
echo "module load OpenSceneGraph-3.4.0-Qt5.5-16.04" >> $HOME/.bashrc
source $HOME/.bashrc
