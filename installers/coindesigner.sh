#!/bin/bash

# Install dependencies
sudo apt-get install libcoin80-dev libassimp-dev \
     libqt4-dev libsoqt4-dev libsimage-dev bison flex

# Build
cd $HOME/opt/src

# As we are downloading master the date will be added
d=`date '+%Y%m%d'`

git clone https://github.com/iocroblab/coindesigner coindesigner-$d
cd coindesigner-$d
mkdir Release
cd Release
cmake .. -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=$HOME/opt/coindesigner-$d
make install

sed -e "s/VERSION/$d/g"  $HOME/opt/modules/.coindesigner \
    > $HOME/opt/modules/coindesigner-$d
ln -s ~/opt/modules/modules/coindesigner-$d ~/opt/modules/installed/coindesigner-$d
echo "module load coindesigner-$d" >> $HOME/.bashrc
