#!/bin/bash

sudo apt install libpoco-dev libx264-dev libavutil-dev libavcodec-dev libavformat-dev libavdevice-dev libavfilter-dev libswscale-dev libpostproc-dev libswresample-dev libpng16-dev

cd $HOME/opt/src
rm -rf $HOME/opt/src/webstreamer
git clone https://github.com/HBPVIS/webstreamer
cd webstreamer
mkdir Release && cd Release
cmake .. -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$HOME/opt/webstreamer . -DCMAKE_PREFIX_PATH=$POCO_DIR
make
# TODO: run install
#make install
ln -s ~/opt/modules/modules/webstreamer ~/opt/modules/installed/webstreamer
