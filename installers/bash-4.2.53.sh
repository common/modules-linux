#!/bin/bash

export BASH_VERSION=4.2.53
sudo apt-get install flex bison
cd $HOME/opt/src
wget http://ftp.gnu.org/gnu/bash/bash-${BASH_VERSION}.tar.gz
tar zxvf bash-${BASH_VERSION}.tar.gz
cd bash-${BASH_VERSION}
export PREFIX=$HOME/opt/bash-${BASH_VERSION}
./configure --prefix=$PREFIX                                 \
            --bindir=$PREFIX/bin                             \
            --htmldir=$PREFIX/share/doc/bash-${BASH_VERSION} \
            --without-bash-malloc                            \
            --with-installed-readline
NUM_PROC=`grep processor /proc/cpuinfo  | wc -l`
NUM_JOBS=`echo $NUM_PROC - 1 | bc`
make -j $NUM_JOBS install
ln -s ~/opt/modules/modules/bash-${BASH_VERSION} ~/opt/modules/installed/bash-${BASH_VERSION}
