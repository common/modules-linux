#!/bin/bash
set -e
echo "Please put your sudo pass to create /home/opt"
sudo mkdir -p /home/opt && sudo chmod 777 /home/opt
cd /home/opt
wget http://gmrv.es/~ptoharia/qt/Qt-5.10.0.tgz
tar zxvf Qt-5.10.0.tgz
rm Qt-5.10.0.tgz
ln -s /home/opt/Qt-5.10.0  /home/opt/Qt5.10.0
ln -s ~/opt/modules/modules/Qt-5.10.0 ~/opt/modules/installed/Qt-5.10.0
echo "module load Qt-5.10.0" >> $HOME/.bashrc
