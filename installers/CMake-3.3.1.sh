#!/bin/bash
# Download, compile and install cmake 3.3.1
cd $HOME/opt/src
wget http://www.cmake.org/files/v3.3/cmake-3.3.1.tar.gz
tar zxvf cmake-3.3.1.tar.gz
cd cmake-3.3.1
mkdir -p Release
cd Release
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/opt/cmake-3.3.1
make install
ln -s ~/opt/modules/modules/cmake-3.3.1 ~/opt/modules/installed/cmake-3.3.1
echo "module load cmake-3.3.1" >> $HOME/.bashrc
source $HOME/.bashrc
