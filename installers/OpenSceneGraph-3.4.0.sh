#!/bin/bash
mkdir -p $HOME/opt/src && cd ~/opt/src
wget http://trac.openscenegraph.org/downloads/developer_releases/OpenSceneGraph-3.4.0.zip
unzip OpenSceneGraph-3.4.0.zip
mkdir OpenSceneGraph-3.4.0/Release && cd OpenSceneGraph-3.4.0/Release
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/opt/OpenSceneGraph-3.4.0 -DCMAKE_BUILD_TYPE=Release
make install
#Fix for CMake scripts to work
cd $HOME/opt/OpenSceneGraph-3.4.0
ln -s lib64 lib
ln -s ~/opt/modules/modules/OpenSceneGraph-3.4.0 ~/opt/modules/installed/OpenSceneGraph-3.4.0
echo "module load OpenSceneGraph-3.4.0" >> $HOME/.bashrc
source $HOME/.bashrc
