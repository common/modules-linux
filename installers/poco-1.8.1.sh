#!/bin/bash
#Install dependences
sudo apt install openssl libssl-dev
sudo apt install libiodbc2 libiodbc2-dev
sudo apt install libmysqlclient-dev

cd $HOME/opt/src
#wget https://pocoproject.org/releases/poco-1.8.1/poco-1.8.1.tar.gz
#tar zxvf poco-1.8.1.tar.gz
#cd poco-1.8.1
git clone https://github.com/pocoproject/poco
cd poco
git checkout poco-1.8.1-release

cd build
cmake -DCMAKE_BUILD_TYPE=Release  -DCMAKE_INSTALL_PREFIX=$HOME/opt/poco-1.8.1 ..
make -j install
ln -s ~/opt/modules/modules/poco-1.8.1 ~/opt/modules/installed/poco-1.8.1
