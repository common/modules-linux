#!/bin/bash

# Stop if errors found
set -e

sudo apt-get install build-essential libxpm-dev libgif-dev

mkdir -p ~/opt/src && cd ~/opt/src
if [ -f emacs-24.5.tar.gz ]; then
    echo "emacs-24.5.tar.gz already exists, not downloading"
else
    wget http://ftp.gnu.org/gnu/emacs/emacs-24.5.tar.gz
fi
tar zxvf emacs-24.5.tar.gz
cd emacs-24.5
./configure --prefix=$HOME/opt/emacs-24.5
make install -j4
ln -s ~/opt/modules/modules/emacs-24.5 ~/opt/modules/installed/emacs-24.5
echo "module load emacs-24.5" >> $HOME/.bashrc
source $HOME/.bashrc
