#!/bin/bash
set -e
echo "Please put your sudo pass to create /home/opt"
sudo mkdir -p /home/opt && sudo chmod 777 /home/opt
cd /home/opt
wget http://gmrv.es/~ptoharia/qt/Qt-5.9.tgz
tar zxvf Qt-5.9.tgz
rm Qt-5.9.tgz
ln -s ~/opt/modules/modules/Qt-5.9 ~/opt/modules/installed/Qt-5.9
echo "Please add \"module load Qt-5.9\" to your .bashrc to load it by default"
