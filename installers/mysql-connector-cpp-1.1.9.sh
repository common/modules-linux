#!/bin/bash
mkdir -p cd $HOME/opt/src && cd $HOME/opt/src
VER=1.1.9
git clone https://github.com/mysql/mysql-connector-cpp mysql-connector-cpp-$VER
cd mysql-connector-cpp-$VER
git checkout $VER
mkdir Release && cd Release
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/opt/mysql-connector-cpp-$VER
make -j install
ln -s ~/opt/modules/modules/mysql-connector-cpp-$VER ~/opt/modules/installed/mysql-connector-cpp-$VER
