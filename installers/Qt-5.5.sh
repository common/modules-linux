#!/bin/bash
cd $HOME/opt
wget http://gmrv.es/~ptoharia/qt/Qt-5.5.tgz
tar zxvf Qt-5.5.tgz
rm Qt-5.5.tgz
ln -s ~/opt/modules/modules/Qt-5.5 ~/opt/modules/installed/Qt-5.5
echo "module load Qt-5.5" >> $HOME/.bashrc
source $HOME/.bashrc
