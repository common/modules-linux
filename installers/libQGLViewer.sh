#!/bin/bash
sudo apt install libqt5designer5 qttools5-dev
mkdir -p ~/opt/src && cd ~/opt/src
wget http://gmrv.es/~ptoharia/sw/libQGLViewer-2.6.3.tar.gz
tar zxvf libQGLViewer-2.6.3.tar.gz
cd libQGLViewer-2.6.3
qmake
make -j4 
INSTALL_ROOT=~/opt/libQGLViewer-2.6.3 make install
ln -s ~/opt/modules/modules/libQGLViewer-2.6.3 ~/opt/modules/installed/libQGLViewer-2.6.3
echo "module load libQGLViewer-2.6.3" >> $HOME/.bashrc
source $HOME/.bashrc
