#!/bin/bash
# Download, compile and install cmake 3.3.1
cd $HOME/opt/src
wget http://www.cmake.org/files/v3.13/cmake-3.13.4.tar.gz
tar zxvf cmake-3.13.4.tar.gz
cd cmake-3.13.4
mkdir -p Release
cd Release
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/opt/cmake-3.13.4
make install $1
ln -s ~/opt/modules/modules/cmake-3.13.4 ~/opt/modules/installed/cmake-3.13.4
#echo "module load cmake-3.13.4" >> $HOME/.bashrc
#source $HOME/.bashrc
