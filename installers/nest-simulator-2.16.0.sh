#!/bin/bash
echo "Installing cython ..."
sudo apt install cython

mkdir -p cd $HOME/opt/src/

wget https://github.com/nest/nest-simulator/archive/v2.16.0.tar.gz
tar xf v2.16.0.tar.gz && rm -f v2.16.0.tar.gz
cd nest-simulator-2.16.0


mkdir nest-build
cd nest-build
cmake -DCMAKE_INSTALL_PREFIX:PATH=$HOME/opt/nest-simulator-2.16.0 ..

make -j ${CORES_TO_BUILD} install

ln -s ~/opt/modules/modules/nest-simulator-2.16.0 ~/opt/modules/installed/nest-simulator-2.16.0
