#!/bin/bash

# Install GNU modules
sudo apt-get install environment-modules

# Create directory for sources
mkdir -p $HOME/opt/src

# Make module available and set MODULEPATH
echo "source /etc/profile.d/modules.sh" >> $HOME/.bashrc
echo "export MODULEPATH=\$HOME/opt/modules/installed:\$MODULEPATH" >> $HOME/.bashrc
